import logging

from flask import Flask, render_template
from flask_assistant import Assistant, ask
from jikanpy import Jikan

from summarizer.Summarizer import summarize_content
from utils.MALScraper import MALScraper
from utils.utils import split_summary_to_match_google_assistant_limitation, extract_anime_id

jikan = Jikan()
app = Flask(__name__)
assist = Assistant(app, route='/')

app.config['ASSIST_ACTIONS_ON_GOOGLE'] = True

cache_result_anime_id = {}
cache_review = {}


def get_summary_response(anime_id):
    resp = ""
    try:
        if anime_id not in cache_result_anime_id:
            result = jikan.anime(anime_id)
            cache_result_anime_id[anime_id] = result

        anime_url = cache_result_anime_id[anime_id]['url']
        anime_title = cache_result_anime_id[anime_id]['title']

        if anime_id not in cache_review:
            cache_review[anime_id] = MALScraper.get_reviews(anime_url)

        reviews = cache_review[anime_id]

        if len(reviews) == 0:
            raise Exception()

        summary = summarize_content(reviews[0], 7)
        summary = split_summary_to_match_google_assistant_limitation(summary)

        resp = ask("Here's the review of anime with MAL ID " + str(anime_id))
        resp.card(summary, "{}'s Review".format(anime_title), None, None, 'MAL ID {}'.format(anime_id),
                  linkTitle="Link to full review", link=anime_url + "/reviews")

    except Exception as e:
        resp = ask("I'm sorry. Review on anime with ID: {} is not found on MyAnimeList".format(anime_id))
        logging.log(logging.ERROR, str(e))

    return resp


def get_carousel_of_anime_response(anime_title, search_results):
    resp = ask("Here's a list of anime with title " + anime_title).build_carousel()
    for result in search_results[:10]:
        key, title, img_url = "MAL ID {}".format(result['mal_id']), result["title"], result['image_url']
        description = "{} - {} - {} episodes".format(title, result['type'], result['episodes'])
        resp.add_item(title=key, key=key, img_url=img_url, description=description)
    return resp


@assist.action('greeting')
def greet_and_start():
    speech = "Hey there! Can I help you today?"
    return ask(speech)


@assist.action('give-anime_title')
def ask_for_anime_review(anime_title):
    search_results = jikan.search("anime", anime_title)['results']
    for result in search_results:
        cache_result_anime_id[result["mal_id"]] = result
    if len(search_results) == 1:
        response = get_summary_response(search_results[0]['mal_id'])
    else:
        response = get_carousel_of_anime_response(anime_title, search_results)
    return response


@assist.action('give-number', mapping={'number': 'sys.number'})
def ask_for_anime_review_summary(number):
    return get_summary_response(number)


@assist.action('give-anime_title - custom', mapping={'OPTION': 'sys.number'})
def ask_for_anime_review_summary_v2(OPTION):
    id = extract_anime_id(OPTION)
    return get_summary_response(id)


@assist.prompt_for('anime_title', intent_name='give-anime_title')
def prompt_anime_title(anime_title):
    speech = "Sorry, I can't understand. Please try again"
    return ask(speech)


@assist.action('ask-for-summarizer-algorithm-evaluation-score')
def ask_for_summarizer_algorithm_evaluation_score():
    from summarizer.Evaluator import get_rouge_metrics
    return ask(str(get_rouge_metrics()))


@app.route('/tos')
def tos():
    return render_template("tos.html")


@app.route('/privacy-policy')
def privacy_policy():
    return render_template("privacy_policy.html")


if __name__ == '__main__':
    logging.getLogger('flask_assistant').setLevel(logging.DEBUG)
    app.run(debug=True)
