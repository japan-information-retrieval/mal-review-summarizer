# MAL Review Summarizer
Information Retrieval 2018/2019 Final Project

## Software Testing Manual
For testing this application without setting up your own environment, refer to [this manual](https://docs.google.com/document/d/1a2CRbrUtYVJbIBg7SYzKzWX0e2Bv4u0YFwu26Fw3H8Y/edit?usp=sharing)

## On Creating Google Assistant with Python
- To run your own instance of Google Assistant, please refer to [this doc](https://flask-assistant.readthedocs.io/en/latest/quick_start.html)

## Getting Started (Run on Local and Host own Dialogflow)
1. Make sure you have pip installed (Python >= 3.5) and [ngrok](https://ngrok.com/)
2. `git clone https://gitlab.com/japan-information-retrieval`
3. `python3 -m venv venv`
4. `source venv/bin/activate && pip3 install -r requirements.txt`
5. `python3 app.py`
6. `ngrok http 5000`
7. Copy output url of ngrok and put it in your Dialogflow fulfilment

## Folder Structure and Explanation
1. `app.py`: Flask application that handles all the webhook logic for Google Assistant (Controller)
2. **`summarizer`**: The implementation of summarizer algorithm and evaluation of the summarizer using ROUGE
3. `utils`: Helpers method
4. `templates`: Dialogflow templates and html templates
5. `schema`: Dialogflow models and config

## About Us
- Albertus Angga Raharja (1606918401)
- Tatag Aziz Prawiro (1606875794)
