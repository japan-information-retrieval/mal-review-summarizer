import os

from rouge import Rouge

from summarizer.Summarizer import summarize_content

ORIGINAL_DOCS_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "docs"))
GOLD_SUMMARIES_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), "gold"))

rouge = Rouge()


def get_rouge_metrics():
    metrics = {}

    file_numbers = 0

    for (dirpath, dirnames, filenames) in os.walk(ORIGINAL_DOCS_DIRECTORY):
        for file in filenames:
            file_numbers += 1

            path = dirpath + "/" + file
            gold_path = os.path.join(dirpath, "..", "gold", file.replace(".mainbody", ".gold"))

            with open(path, "r") as text, open(gold_path, "r") as gold:
                original_text = "".join(text.readlines())

                gold_text = gold.readlines()
                gold_length_of_sentence = len(gold_text)
                gold_text = "".join(gold_text)

                hypothesis = summarize_content(original_text, gold_length_of_sentence)

                rouge_result = rouge.get_scores(hypothesis, gold_text, True)

                for key in rouge_result:
                    if key not in metrics:
                        metrics[key] = rouge_result[key]
                    else:
                        for key2 in rouge_result[key]:
                            metrics[key][key2] += rouge_result[key][key2]
    for key in metrics:
        for key2 in metrics[key]:
            metrics[key][key2] /= file_numbers

    return metrics