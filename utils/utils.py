from nltk.tokenize import word_tokenize, sent_tokenize

GOOGLE_ASSITANT_SIMPLE_RESPONSE_LENGTH_LIMIT = 750


def is_anime_id(query):
    return query.isdigit()


def extract_anime_id(query):
    words = word_tokenize(query)
    for word in words:
        if is_anime_id(word):
            return word
    return "1"


def split_summary_to_match_google_assistant_limitation(summary):
    sentences, response = sent_tokenize(summary), []
    cur = ""
    for sentence in sentences:
        if len(cur) + len(sentence) <= GOOGLE_ASSITANT_SIMPLE_RESPONSE_LENGTH_LIMIT:
            cur += sentence
        else:
            response.append(cur)
            cur = ""
    return response[0]
