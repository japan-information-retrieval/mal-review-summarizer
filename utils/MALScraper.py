import urllib3
from bs4 import BeautifulSoup


class MALScraper:
    @staticmethod
    def get_reviews(anime_url, page=1):
        anime_url += "/reviews?p={}".format(page)
        html_page = urllib3.PoolManager().request('GET', anime_url)
        soup = BeautifulSoup(html_page.data)
        reviews = []
        for result in soup.find_all("div", "spaceit textReadability word-break pt8 mt8"):
            for i in range(2):
                if result.a:
                    result.a.extract()
            if result.table:
                result.table.extract()
            reviews.append(result.text.strip())
        return reviews
